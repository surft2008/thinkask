<?php 
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\behavior;
use think\Db;
use \think\Request;
use \think\Session;
use \think\Cookie;
use \think\Hook;
use \think\Lang;
// echo "当前模块名称是" . $request->module();
// echo "当前控制器名称是" . $request->controller();
// echo "当前操作名称是" . $request->action();
class Inithook
{
    public function run(&$params)
    {
       $data = cache('hooks');
        if(!$data){
           $plugins =Db::name('plus')->where('status',1)->field('name,hooks')->select();
           if(!empty($plugins)){
               foreach ($plugins as $plugin => $hooks) {
                   if($hooks){
                      Hook::add($hooks['hooks'],$hooks['name']);
                   }
               }
           }
           cache('hooks',Hook::get());
        }else{
           Hook::import($data,false);
        }
    }


}