<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\ucenter\controller;
use app\common\controller\Base;
use app\ajax\model\User as askuser;
class Ajax extends Base
{

    public function changeinfo(){
    	// show($this->request->param());
    	// die;
        if($this->request->isAJax()&&$this->getuid()>0){
            $where = $this->request->only(['uid']);
            $password = $this->request->only(['password']);
            $data = $this->request->param();
            $data['salt'] = rand_str(4);
            if(empty($password['password'])){
            	unset($data['password']);
            }else{
               $data['password']=encode_pwd($password['password'],$data['salt']);
            }
            unset($data['uid']);
            if((int)$where['uid']>0){
                //修改
               $re = model('Base')->getedit('users',['where'=>$where],$data); 
           }else{
                //新加
               $where['uid']=model('Base')->getadd('users',$data);
            }
            
            if(model('Base')->getone('users_attrib',['where'=>$where,'cache'=>false])){
                //修改
                model('Base')->getedit('users_attrib',['where'=>$where],$data);
            }else{

                //新加
                $data['uid'] = $where['uid'];
                model('Base')->getadd('users_attrib',$data);
            }

            $this->success('修改成功','','');
        }

      
    }
    /**
     * [logo 登陆]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function logo()
    {
        if($this->request->isAJax()){
            $user_name = $this->request->only(['user_name']);
            $password = $this->request->only(['password']);
            $returnurl = $this->request->only(['returnurl']);
            if(empty($user_name['user_name'])||empty($password['password'])){
                $this->error('用户名或者密码不能为空');
            }
            $re  = askuser::checkLogin($user_name['user_name']);
            if(is_array($re)&&!empty($re)){
                //比对密码  
                if($re['password']==encode_pwd($password['password'],$re['salt'])){
                    session('thinkask_uid',$re['uid']);
                    // 设置Cookie 有效期为 3600秒
                    cookie('thinkask_uid',$re['uid']);

                    $this->success('登陆成功','',"");
                }else{
                    $this->error('密码错误');
                }
            }else{
                $this->error('用户名不存在');
            }
          
        }

    }
    /**
     * [reg 注册]
     * @return [type] [description]
     */
    public function reg(){
      if($this->request->isAJax()){
        $user_name = tostr($this->request->only(['user_name']));
        $password = tostr($this->request->only(['password']));
        $repassword = tostr($this->request->only(['repassword']));
        $email = tostr($this->request->only(['email']));
        //协议
        $agreement = tostr($this->request->only(['agreement']));
        //用户注册名不允许出现以下关键字:
        $not_user=explode(",", getset('censoruser'));

        // $this->vali($rule,$msg,$this->re)
        if(!$user_name) $this->error('用户名不能为空');
        if(in_array($user_name, $not_user)) $this->error('此用户名已被管理员设置为禁止注册');
        if(strlen($user_name)<getset('username_length_min')) $this->error('用户名不能小于'.getset('username_length_min').'位');
        if(strlen($user_name)>getset('username_length_max')) $this->error('用户名不能大于'.getset('username_length_max').'位');
        if(!filter($email, 'email')) $this->error('邮箱格式错误');
        if(!$password) $this->error('密码不能为空');
        if(strlen($password)<6) $this->error('密码必须大于6位');
        if($password!=$repassword) $this->error('两次密码不一样');
        //是否开启验证码
        if(getset('register_seccode')=="Y"){
          if(!captcha_check(current($this->request->only(['captcha']))))$this->error('验证码错误');  
        }
        //注册协议
        if(!$agreement) $this->error('你必须同意注意协议才可以注册');
        if(model('Base')->getone('users',['where'=>['user_name'=>"{$user_name}"]])) $this->error('用户名已存在');
        if(model('Base')->getone('users',['where'=>['email'=>"{$email}"]])) $this->error('邮箱已存在');
        $data['salt'] = rand_str(4);
        $data['password'] = encode_pwd($password,$salt);
        $data['email'] = $email;
        //是否开启邮箱验证
         if(getset('register_valid_type')=="Y"){ 
            $data['valid_email']=0;
        }else{
            $data['valid_email']=1;
        }
        // die;
        if(model('Base')->getadd('users',$data)){
          $this->success('注册成功',url('ucenter/user/login'));
        }else{
          $this->error('服务器异常,重试');
        }
      }
    }
    /**
     * [logout 退出]
     * @return [type] [description]
     */
    public function logout(){
      if($this->request->isAJax()){
        session('thinkask_uid',null);
        cookie('thinkask_uid',null);
        $this->success('成功退出','/');
      }
    }
}
