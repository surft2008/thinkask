<?php
//SanSanYun.Com [Don't forget the beginner's mind]
//Copyright (c) 2016~2099 http://SanSanYun.com All rights reserved.
//Author: 记忆、del <380822670@qq.com>
namespace app\people\controller;
use app\common\controller\Base;

class People extends Base
{
    public function index(){
        $this->assign(db('Users')->where('uid',session('thinkask_uid'))->find());

        $question_list = db('Question')->where('published_uid',input('param.id'))->order('add_time desc')->limit(12)->select();
        $this->assign('question_list',$question_list);

        return $this->fetch('people/people');
    }

    public function people_list(){
        $this->assign('list',$this->getbase->getpages('users'));

        return $this->fetch('people/people_list');
    }
}